package rs.ac.bg.etf.blinder.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rs.ac.bg.etf.blinder.exceptions.DatabaseException;
import rs.ac.bg.etf.blinder.frontend.LetterRecognizer;
import rs.ac.bg.etf.blinder.frontend.MainActivity;

/**
 * Regulates interface for database.
 *
 * @author Dejan Markovic
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "sounds.db";
    private static final int VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(DatabaseHelper.class.getName(), "Creating database!");
        db.execSQL("CREATE TABLE songs(ID INTEGER PRIMARY KEY AUTOINCREMENT, NAME TEXT NOT NULL, CONTENT BLOB NOT NULL, LANGUAGE TEXT NOT NULL, UNIQUE(NAME,LANGUAGE));");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // drop old version and update with new version of database
        db.execSQL("DROP TABLE IF EXISTS songs");
        this.onCreate(db);
    }

    /**
     * Kesira audio fajl u bazi za buduce koriscenje.
     *
     * @param file Fajl u kom se nalazi audio fajl
     * @throws IOException       baca se u slucaju da postoji problem sa citanjem fajla
     * @throws DatabaseException baca se u slucaju da postoji problem u komunikaciji sa bazom
     */
    public void insert(File file) throws IOException, DatabaseException {
        if (file == null) throw new DatabaseException("Input object is null!");
        Log.i(DatabaseHelper.class.getName(), "Inserting in database: " + file.getName());
        SQLiteDatabase db = getWritableDatabase();// make data for new line of data in table
        ContentValues cv = new ContentValues();
        cv.put(Columns.NAME.name(), file.getName().replaceAll("%20", "_"));
        byte[] data = read(file);
        cv.put(Columns.CONTENT.name(), data);
        cv.put(Columns.LANGUAGE.name(), LetterRecognizer.getLanguage());
        db.insert("songs", null, cv);
        Log.i(DatabaseHelper.class.getName(), "Success insert!");
    }

    public static byte[] read(File file) throws IOException{
        ByteArrayOutputStream ous = null;
        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        }finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }

    /**
     * Pokusava da dohvati kesiranu pesmu iz baze.
     *
     * @param name ime audio fajla koji se dohvata
     * @return vraca fajl sa pesmom ukoliko ona postoji ili null
     * @throws DatabaseException baca se u slucaju da postoji problem u komunikaciji sa bazom
     */
    public File getSong(String name) throws DatabaseException {
        String nm = name.replaceAll("%20", "_") + ".waw";
        String selectQuery = "SELECT CONTENT FROM songs WHERE NAME=? AND LANGUAGE=?";
        Cursor cur = getReadableDatabase().rawQuery(selectQuery, new String[]{nm, LetterRecognizer.getLanguage()});
        Log.i(DatabaseHelper.class.getName(), "Try fetch: " + name + " for language " + LetterRecognizer.getLanguage());
        File file = new File(MainActivity.getInstance().getFilesDir(), nm);
        file.delete();
        if (cur != null) {
            cur.moveToFirst();
            try {
                byte[] data = cur.getBlob(cur.getColumnIndex(Columns.CONTENT.name()));
                FileOutputStream fos = new FileOutputStream(file);
                fos.write(data);
                fos.close();
                Log.i(DatabaseHelper.class.getName(), "We found it in database!");
            } catch (Exception e) {
                Log.i(DatabaseHelper.class.getName(), "Not found in database!");
                throw new DatabaseException("ERROR while getting data from database: " + e.getMessage());
            } finally {
                cur.close();
            }
        }
        return file;
    }

    /**
     * Ciscenje kesiranih fajlova ako se baza povecala
     */
    public void clearDatabase() {
        getWritableDatabase().delete("songs", null, null);
    }

    /**
     * Kolone koje postoje u bazi
     */
    public enum Columns {
        ID, NAME, CONTENT, LANGUAGE
    }

}
