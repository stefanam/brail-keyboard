package rs.ac.bg.etf.blinder.tasks;

/**
 * @author Dejan Markovic
 */
public enum Language {
    en,//english
    de,//german
    es,//spanish
    sr;//serbian
}
