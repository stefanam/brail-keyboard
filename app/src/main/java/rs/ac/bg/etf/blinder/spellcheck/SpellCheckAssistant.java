package rs.ac.bg.etf.blinder.spellcheck;

import rs.ac.bg.etf.blinder.frontend.MainActivity;

/**
 * Manager for custom spellchecking if Android API is forbidden by manufacturer.
 * author Dejan Markovic
 */
public class SpellCheckAssistant {

    private boolean type = false;//lucene or levensthein
    private MainActivity context;


    public SpellCheckAssistant(MainActivity activity, boolean type) {
        this.type = type;
        this.context = activity;
       /* try {
            createIndex();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    /*public void createIndex() throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        boolean recreateIndexIfExists = true;
        IndexWriter indexWriter = new IndexWriter(context.getFilesDir(), analyzer, recreateIndexIfExists);
        File dir = getFileFromRawResource();
        File[] files = dir.listFiles();
        for (File file : files) {
            Document document = new Document();

            String path = file.getCanonicalPath();
            document.add(new Field(context.getFilesDir().getAbsolutePath(), path, Field.Store.YES, Field.Index.UN_TOKENIZED));

            Reader reader = new FileReader(file);
            document.add(new Field(context.getFilesDir().getAbsolutePath(), reader));

            indexWriter.addDocument(document);
        }
        indexWriter.optimize();
        indexWriter.close();
    }

    public void searchIndex(String searchString) throws IOException, ParseException {
        System.out.println("Searching for '" + searchString + "'");
        Directory directory = FSDirectory.getDirectory(context.getFilesDir());
        IndexReader indexReader = IndexReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        Analyzer analyzer = new StandardAnalyzer();
        QueryParser queryParser = new QueryParser(context.getFilesDir().getAbsolutePath(), analyzer);
        Query query = queryParser.parse(searchString);
        Hits hits = indexSearcher.search(query);
        System.out.println("Number of hits: " + hits.length());

        Iterator<Hit> it = hits.iterator();
        while (it.hasNext()) {
            Hit hit = it.next();
            Document document = hit.getDocument();
            String path = document.get(context.getFilesDir().getAbsolutePath());
            System.out.println("Hit: " + path);
        }

    }

    private File getFileFromRawResource() {
        try {
            // Get the InputStream
            InputStream inputStream = context.getResources().openRawResource(R.raw.dictionary);

            File file = new File(context.getFilesDir(), "dictionary.txt");
            if(!file.exists()) {
                FileOutputStream fileOutputStream = new FileOutputStream(file);
                // IOUtils is a class from Apache Commons IO
                // It writes an InputStream to an OutputStream
                IOUtils.copy(inputStream, fileOutputStream);
                fileOutputStream.close();
            }
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getMostSimmilar(String wordToCorrect){
        if(searchIndex(wordToCorrect))return wordToCorrect; //no need to correct
        Directory directory = FSDirectory.getDirectory(context.getFilesDir());
        IndexReader indexReader = IndexReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        Analyzer analyzer = new StandardAnalyzer();
        QueryParser queryParser = new QueryParser(context.getFilesDir().getAbsolutePath(), analyzer);
        int minDistance;
        String minWord = "";
        for(Item i: queryParser.items){
            int distance = LevenshteinDistance.computeDistance(wordToCorrect, i.toString());
            if(distance<minDistance){
                minDistance = distance;
                minWord = i.toString();
            }
        }
        return minWord;
    }
    */

}
