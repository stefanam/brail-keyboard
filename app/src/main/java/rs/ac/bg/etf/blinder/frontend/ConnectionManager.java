package rs.ac.bg.etf.blinder.frontend;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import rs.ac.bg.etf.blinder.R;

/**
 * Proverava stanje internet veze i proverava tip veze.
 *
 * @author Dejan Markovic
 */
public class ConnectionManager {

    private MainActivity context;

    private boolean connected = false;
    private boolean haveConnectedWifi = false;
    private boolean haveConnectedMobile = false;

    public ConnectionManager(MainActivity activity) {
        this.context = activity;
    }

    public boolean isNetAvailable() {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();

            //* Check for availability and if is really connected.
            connected = networkInfo != null && networkInfo.isAvailable() && networkInfo.isConnected();

            //* what kind, Connectivity WIFI or MOBILE?.
            if (networkInfo != null) {
                NetworkInfo[] netInfo = connectivityManager.getAllNetworkInfo();
                for (NetworkInfo ni : netInfo) {
                    if ((ni.getTypeName().equalsIgnoreCase("WIFI")) && ni.isConnected()) {
                        Log.i("Connectivity", " WIFI IS ENABLED");
                        haveConnectedWifi = true;
                    }
                    if ((ni.getTypeName().equalsIgnoreCase("MOBILE")) && ni.isConnected()) {
                        Log.i("Connectivity", " MOBILE IS ENABLED");
                        haveConnectedMobile = true;
                    }
                }
            }

            if (!connected) {
                //notify user that speaking out will not be available
                sayWarningNoInternet();
            }

            return connected;


        } catch (Exception e) {
            Log.e("Exception Connectivity", e.getMessage());
        }
        return connected;
    }

    protected void sayWarningNoInternet() {
        try {
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.setDataSource(getFileFromRawResource(R.raw.no_internet).getPath());
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private File getFileFromRawResource(int id) {
        try {
            InputStream in = context.getResources().openRawResource(id);
            File file = new File(context.getFilesDir(), "no_internet.mp3");
            FileOutputStream out = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = in.read(buffer)) != -1) {
                out.write(buffer, 0, len);
            }
            out.close();
            return file;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
