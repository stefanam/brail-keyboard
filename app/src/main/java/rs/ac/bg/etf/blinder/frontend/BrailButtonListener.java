package rs.ac.bg.etf.blinder.frontend;

import android.view.View;

/**
 * Belezi koje dugme za prepoznavanje tacaka u Brajevoj azbuci je stisnuto.
 *
 * @author Dejan Markovic
 */
public class BrailButtonListener implements View.OnClickListener {

    private MainActivity activity;
    private int index;

    public BrailButtonListener(MainActivity activity, int index) {
        this.activity = activity;
        this.index = index;
    }

    @Override
    public void onClick(View v) {
        new Thread() {

            @Override
            public void run() {
                activity.letterRecognizer.setDot(index);
            }

        }.start();
    }
}
