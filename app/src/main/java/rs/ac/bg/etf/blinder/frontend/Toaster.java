package rs.ac.bg.etf.blinder.frontend;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import rs.ac.bg.etf.blinder.R;

/**
 * Prikazuje full screen toast.
 *
 * @author Dejan Markovic
 */
public class Toaster {

    private static Toast toast;

    public static void showToastLetter(final Activity activity, final char letters, final long length, final int color) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (toast != null) toast.cancel();
                toast = new Toast(MainActivity.getInstance());
                LayoutInflater inflater = (LayoutInflater) activity.
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);

                TextView layout = (TextView) inflater.inflate(R.layout.letter_toast, null);
                layout.setTextColor(color);
                layout.setText(Character.toString(letters));
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.setDuration(Toast.LENGTH_LONG);
                toast.setView(layout);
                toast.show();
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        toast.cancel();
                    }
                }, length);
            }
        });
    }

}
