package rs.ac.bg.etf.blinder.frontend;

import android.graphics.Color;

import rs.ac.bg.etf.blinder.R;
import rs.ac.bg.etf.blinder.exceptions.LetterNotRecognizedException;

/**
 * Pokusava da prepozna rec od unetih slova.
 *
 * @author Dejan Markovic
 */
public class WordRecognizer implements LetterRecognizer.LetterReceiver {

    private static WordRecognizer recognizer;
    private StringBuilder wordBuffer = new StringBuilder();
    boolean caps = false;
    boolean number = false;
    private MainActivity activity;
    private boolean block;

    private WordRecognizer() {
        activity = MainActivity.getInstance();
    }

    public static final WordRecognizer getWordRecognizer() {
        if (recognizer == null) {
            recognizer = new WordRecognizer();
        }
        return recognizer;
    }

    /**
     * Apenduje prepoznato slovo na kraj tekuce reci.
     *
     * @param letter prepoznato slovo
     */
    @Override
    public void appendLetter(char letter) {
        if (letter == ' ') {
            // word grouped
            if (wordBuffer.length() == 0) return; // space only pressed
            caps = false;
            number = false;
            if (!block) MainActivity.getInstance().setCurrentWord(parseBuffer(wordBuffer));
            wordBuffer = new StringBuilder();
            Toaster.showToastLetter(MainActivity.getInstance(), '_', 1000, Color.RED);
        } else if (letter == '<') {//delete one letter
            wordBuffer.append(letter);
            activity.getcache().tryFindOrDownload(MainActivity.getInstance().getString(R.string.delete), true);
            activity.vibrate(MainActivity.SHORT_VIBRATE);
            Toaster.showToastLetter(MainActivity.getInstance(), '<', 1000, Color.RED);
        } else {
            if (letter == '#') {
                caps = !caps;
                activity.getcache().tryFindOrDownload(MainActivity.getInstance().getString(R.string.big) +
                        "%20" + (caps ? MainActivity.getInstance().getString(R.string.start) :
                        MainActivity.getInstance().getString(R.string.end)), true);
            } else if (letter == '$') {
                number = !number;
                activity.getcache().tryFindOrDownload(MainActivity.getInstance().getString(R.string.number) +
                        "%20" + (number ? MainActivity.getInstance().getString(R.string.start) :
                        MainActivity.getInstance().getString(R.string.end)), true);
            } else {
                try {
                    char l = (number ? getNumber(letter) : caps ? Character.toUpperCase(letter) : letter);
                    Toaster.showToastLetter(MainActivity.getInstance(), l, 1000, Color.RED);
                    activity.getcache().tryFindOrDownload(l + "", true);
                } catch (LetterNotRecognizedException e) {
                    activity.startLongSpeakingSession(MainActivity.getInstance().getString(R.string.not_a_number).replaceAll(" ", "%20"));
                    e.printStackTrace();
                    return;
                }
            }
            if (!block || (block && (letter == '#' || letter == '$'))) wordBuffer.append(letter);
            activity.vibrate(MainActivity.SHORT_VIBRATE);
        }
    }

    /**
     * Kada se pritisne razmak, rec se parsira i prate se slova da li su veliko ili malo slovo broj i slicno.
     *
     * @param s StringBuilder sa reci koja se parsira
     * @return parsirani string
     */
    private String parseBuffer(final StringBuilder s) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < s.length(); i++) {
            char c = s.charAt(i);
            switch (c) {
                case '#':
                    caps = !caps;
                    break;
                case '$':
                    number = !number;
                    break;
                default:
                    if (c == '<') {
                        builder.append(c);
                    } else if (!caps && !number) {
                        builder.append(c);
                    } else if (caps && !number) {
                        builder.append(Character.toUpperCase(c));
                    } else if (!caps && number) {
                        try {
                            builder.append(getNumber(c));
                        } catch (LetterNotRecognizedException e) {
                            e.printStackTrace();
                        }
                    } else {
                        //error
                        // skip letters, maybe auto-correct will work
                    }
            }
        }
        return builder.toString().replaceAll("[a-zA-Z0-9]<", "");
    }

    /**
     * CKonvertuje slovo u broj ako je ukljucen rezim brojeva.
     *
     * @param c slovo koje se konvertuje
     * @return number brojcani reprezent slova
     * @throws LetterNotRecognizedException baca se u slucaju da je prosledjeno slovo nije broj
     */
    private char getNumber(char c) throws LetterNotRecognizedException {
        switch (c) {
            case 'a':
                return '1';
            case 'b':
                return '2';
            case 'c':
                return '3';
            case 'd':
                return '4';
            case 'e':
                return '5';
            case 'f':
                return '6';
            case 'g':
                return '7';
            case 'h':
                return '8';
            case 'i':
                return '9';
            case 'j':
                return '0';
        }
        throw new LetterNotRecognizedException(c);
    }

    public void blockInput() {
        block = true;
    }

    public void unblockInput() {
        block = false;
    }
}
